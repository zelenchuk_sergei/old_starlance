from django.core.mail import EmailMultiAlternatives
from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from callback.forms import EmailLists, CourseOrder
from courses.models import CourseItem, CourseProgramTitle
from callback.models import EmailLists as EmailListsModel

from django.core import serializers
from django.http import HttpResponse


def homepage(request):
    form = EmailLists(request.POST or None)
    emails_users = EmailListsModel.objects.all();

    if request.method == 'POST':
        if form.is_valid():

            # if emails_users empty we save user email in database and send email width link
            if not emails_users:
                form.save()


                try:
                    # first letter to admin
                    subject, from_email, to = 'Новый подписчик на email-рассылку', 'zelenchyks@gmail.com', 'zelenchyks@gmail.com'
                    text_content = 'Новый подписчик'
                    html_content = '''
                        <h1>Новый подписчик на email-рассылку</h1>
                        <table>
                             <tr>
                                <td><b>Email:</b></td>
                                <td>&nbsp;&nbsp;%s</td>
                            </tr>
                            
                        </table>
                        ''' % (request.POST['email'])

                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    # second letter to user
                    subject, from_email, to = 'Благодарим за подписку', 'zelenchyks@gmail.com', request.POST['email']
                    text_content = 'Благодарим за подписку'

                    html_content = '''
                        <h3>Благодарим за подписку</h3>
                        <p>Ваша почта: <strong>%s</strong>, успешно добавленна в нашу базу.</p>

                        <br>

                        <p>
                            Ссылка на первый системный и <strong>бесплатный видеоурок</strong> - 
                            <a href='https://youtu.be/wWWOSBDtgQ0'>
                                Пишем свой первый сайт
                            </a>
                        </p>

                        <br>

                        <p>
                            Закажите курс <strong>"HTML & CSS. Первый сайт"</strong> прямо сейчас и получите <strong>
                            скидку в размере 400 грн</strong>. Менеджер проверит в базе, что Вы подписаны на рассылку и
                            скидка Ваша.
                            <br>
                            <br>
                            
                            &#8681;&#8681;&#8681;&#8681;&#8681;&#8681;&#8681;&#8681;&#8681;&#8681;&#8681;&#8681;&#8681;
                            <br>
                            <a href='https://starlance.com.ua/courses/html-css-begin'>
                                Программа курса "HTML & CSS. Первый сайт" 
                            </a>
                        </p>

                        <br>

                        <p>
                            С уважением, Зеленчук Сергей.
                        </p>

                        <br>

                        <a href="https://www.instagram.com/starlanceschool/">Instagram StarLance School</a>


                        ''' % (request.POST['email'])

                    msg_with_link = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg_with_link.attach_alternative(html_content, "text/html")
                    msg_with_link.send()

                except Warning:
                    print('Huston we have a problems with smtp')

                request.session['order_type'] = 'email'
                return redirect('thank_you_page')

            else:
                # if emails_users not empty we go check isset current email in db
                for email in emails_users:
                    if request.POST['email'] != email.email:
                        form.save()
                
                        try:
                            # first form 
                            subject, from_email, to = 'Новый подписчик на email-рассылку', 'zelenchyks@gmail.com', 'zelenchyks@gmail.com'
                            text_content = 'Новый подписчик'
                            html_content = '''
                                <h1>Новый подписчик на email-рассылку</h1>
                                <table>
                                     <tr>
                                        <td><b>Email:</b></td>
                                        <td>&nbsp;&nbsp;%s</td>
                                    </tr>
                                    
                                </table>
                                ''' % (request.POST['email'])

                            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                            msg.attach_alternative(html_content, "text/html")
                            msg.send()
                            # end first form 


                            # second letter to user
                            subject, from_email, to = 'Благодарим за подписку', 'zelenchyks@gmail.com', request.POST['email']
                            text_content = 'Благодарим за подписку'

                            html_content = '''
                                <h3>Благодарим за подписку</h3>
                                <p>Ваша почта: <strong>%s</strong>, успешно добавленна в нашу базу.</p>

                                <p>
                                    Ссылка на первый системный и <strong>бесплатный видеоурок</strong> - 
                                    <a href='https://youtu.be/wWWOSBDtgQ0'>
                                        Пишем свой первый сайт
                                    </a>
                                </p>

                                <p>
                                    Закажите курс <strong>"HTML & CSS. Первый сайт"</strong> прямо сейчас и получите <strong>
                                    скидку в размере 400 грн</strong>. Менеджер проверит в базе, что Вы подписаны на рассылку и
                                    скидка Ваша.
                                    <br>
                                    <br>
                                    
                                    &#8681;&#8681;&#8681;&#8681;&#8681;&#8681;&#8681;&#8681;&#8681;&#8681;&#8681;&#8681;&#8681;
                                    <br>
                                    <a href='https://starlance.com.ua/courses/html-css-begin'>
                                        Программа курса "HTML & CSS. Первый сайт" 
                                    </a>
                                </p>

                                <br>

                                <p>
                                    С уважением, Зеленчук Сергей.
                                </p>

                                <a href="https://www.instagram.com/starlanceschool/">Instagram StarLance School</a>


                                ''' % (request.POST['email'])

                            msg_with_link = EmailMultiAlternatives(subject, text_content, from_email, [to])
                            msg_with_link.attach_alternative(html_content, "text/html")
                            msg_with_link.send()
                        except Warning:
                            print('Huston we have a problems with smtp')

                        request.session['order_type'] = 'email'
                        return redirect('thank_you_page')
                    else:
                        return render(request, 'homepage.html', {
                            'form': form,
                            'subscriber': True
                        })

    return render(request, 'homepage.html', {
        'form': form,
    })


def get_ty_page(request):

    # if we have order_type in request.session we render other title
    if 'order_type' in request.session:
        type = request.session['order_type']

        print('\n', request.session['order_type'], '\n' )

        # delete session key
        del request.session['order_type']

        return render(request, 'ty_page.html', {
            'subscriber': True
        })

    return render(request, 'ty_page.html')


def courses_list(request):
    courses = get_list_or_404(CourseItem)

    return render(request, 'courses_list.html', {
        "courses": courses
    })


def course_detail(request, slug):
    course = get_object_or_404(CourseItem, slug=slug)
    course_program_titles = get_list_or_404(CourseProgramTitle, course=course.pk)

    return render(request, 'course_detail.html', {
        "course": course,
        "program_titles": course_program_titles,
    })
    

def course_order(request, course_slug):
    course = get_object_or_404(CourseItem, slug=course_slug)

    form = CourseOrder(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():
            form.save()

            try:
                subject, from_email, to = 'Новая заявка', 'zelenchyks@gmail.com', 'zelenchyks@gmail.com'
                text_content = 'Новая заявка'
                html_content = '''
                    <h1>Заявка на курс %s</h1>
                    <table>
                         <tr>
                            <td><b>Имя клиента:</b></td>
                            <td>&nbsp;&nbsp;%s</td>
                        </tr>

                         <tr>
                            <td><b>Мобильный:</b></td>
                            <td>&nbsp;&nbsp;%s</td>
                        </tr>

                         <tr>
                            <td><b>Email:</b></td>
                            <td>&nbsp;&nbsp;%s</td>
                        </tr>
                        
                    </table>
                    ''' % (course.title, request.POST['name'], request.POST['mobile'], request.POST['email'])

                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
            except Warning:
                print('Huston we have a problems with smtp')

            return redirect('thank_you_page')

    return render(request, "course_order.html", {
        "course": course,
        'form': form,
    })


def get_all_courses(request):
    courses = get_list_or_404(CourseItem, visible=True)
    data = serializers.serialize('json', courses)
    return HttpResponse(data, content_type='application/json')
