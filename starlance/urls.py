"""starlance URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
# from django.views.generic import TemplateView
# from django.conf.urls import url
# from .views import homepage, get_ty_page, courses_list, course_detail, course_order, get_all_courses

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('', homepage, name="homepage"),
    # path('thank_you/', get_ty_page, name="thank_you_page"),
    #
    # path('courses/', courses_list, name="courses_list"),
    # path('courses/<slug:slug>', course_detail, name="course_detail"),
    #
    # path('order/<slug:course_slug>', course_order, name="course_order"),
    #
    #
    # path('api/get_all_courses', get_all_courses),

    # url(r'^$', TemplateView.as_view(template_name='index.html')),

    path('v1/', include('api.urls')),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
