from django.urls import path, include

from . import views

urlpatterns = [
    path('courses/', views.ListCourseItem.as_view()),
    path('courses/<slug:slug>/', views.DetailCourseItem.as_view()),

    path('program-titles-for-course-id/<int:pk>/', views.DetailCourseProgramTitle.as_view()),

    path('course-order/', views.CourseOrder.as_view()),

    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
]