from rest_framework import serializers
from courses.models import CourseItem, CourseProgramTitle
from callback.models import CourseOrder


class CourseItemSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'slug',
            'title',
            'title_ua',
            'hours',
            'through_price_person',
            'price_person',
            'through_price_group',
            'price_group',
            'description',
            'description_ua',

            'course_img',

            'seo_title',
            'seo_title_ua',

            'seo_description',
            'seo_description_ua',

            'certificate',
            'created',
            'visible',
        )
        model = CourseItem
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }


class CourseProgramTitleSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'course',
            'title',
            'title_ua',
            'description',
            'description_ua',
        )
        model = CourseProgramTitle


class OrderCoursesSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'name',
            'mobile',
            'email',
            'course',
        )
        model = CourseOrder
