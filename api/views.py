from rest_framework import generics, pagination, permissions

from courses import models as Models
from . import serializers
from django.core.mail import EmailMultiAlternatives


# update pagination page returned
class LargeResultsSetPagination(pagination.PageNumberPagination):
    page_size = 10000
    page_size_query_param = 'page_size'
    max_page_size = 20000


# ------------------------------------------------


class ListCourseItem(generics.ListCreateAPIView):
    queryset = Models.CourseItem.objects.all()
    serializer_class = serializers.CourseItemSerializer


class DetailCourseItem(generics.RetrieveUpdateDestroyAPIView):
    queryset = Models.CourseItem.objects.all()
    lookup_field = 'slug'
    serializer_class = serializers.CourseItemSerializer


class DetailCourseProgramTitle(generics.ListAPIView):
    serializer_class = serializers.CourseProgramTitleSerializer
    lookup_url_kwarg = "pk"

    def get_queryset(self):
        course = self.kwargs.get(self.lookup_url_kwarg)
        program_titles = Models.CourseProgramTitle.objects.all().filter(course=course)

        return program_titles


class CourseOrder(generics.CreateAPIView):
    serializer_class = serializers.OrderCoursesSerializer
    permission_classes = [permissions.AllowAny]

    # https://www.django-rest-framework.org/api-guide/generic-views/#methods
    def perform_create(self, serializer):
        # print(serializer['name'].value)

        serializer.save()

        try:
            subject, from_email, to = 'Новая заявка', 'zelenchyks@gmail.com', 'zelenchyks@gmail.com'
            text_content = 'Новая заявка'
            html_content = '''
                <h1>Заявка на курс</h1>
                <table>
                     <tr>
                        <td><b>Имя клиента:</b></td>
                        <td>&nbsp;&nbsp;%s</td>
                    </tr>

                     <tr>
                        <td><b>Мобильный:</b></td>
                        <td>&nbsp;&nbsp;%s</td>
                    </tr>

                     <tr>
                        <td><b>Email:</b></td>
                        <td>&nbsp;&nbsp;%s</td>
                    </tr>

                </table>
                ''' % (serializer['name'].value, serializer['mobile'].value, serializer['email'].value)

            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
        except Warning:
            print('Huston we have a problems with smtp')
