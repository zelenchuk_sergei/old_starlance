from django.db import models


class CourseItem(models.Model):
    slug = models.SlugField('ЧПУ')
    css_class = models.CharField('ClassName CSS', max_length=255)

    title = models.CharField('Имя RUS', max_length=255)
    title_ua = models.CharField('Имя UA', max_length=255, blank=True)

    hours = models.CharField('Кол-во часов', max_length=255)

    through_price_person = models.CharField('(Инд) Цена без скидки', max_length=255)
    price_person = models.CharField('(Инд) Последняя цена', max_length=255)

    through_price_group = models.CharField('(Группа) Цена без скидки', max_length=255)
    price_group = models.CharField('(Группа) Последняя цена', max_length=255)

    description = models.TextField('Описание курса RUS')
    description_ua = models.TextField('Описание курса UA', blank=True)

    course_img = models.ImageField('Изображение курса', upload_to="course_img/%Y/%m/%d/", default='', blank=True, null=True)
    certificate = models.ImageField('Сертификат', upload_to="certificate/%Y/%m/%d/", default='', blank=True)

    seo_title = models.CharField('SEO-title RU', max_length=255, blank=True, null=True,)
    seo_title_ua = models.CharField('SEO-title UA', max_length=255, blank=True, null=True)

    seo_description = models.TextField('SEO-Описание курса RUS', null=True, blank=True)
    seo_description_ua = models.TextField('SEO-Описание курса UA', blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    visible = models.BooleanField("Показан?", blank=True, null=True)

    class Meta:
        ordering = ['id']
        verbose_name = "Курс"
        verbose_name_plural = 'Курсы'

    def __str__(self):
        return self.title


class CourseProgramTitle(models.Model):
    course = models.ForeignKey(CourseItem, on_delete=models.CASCADE)
    title = models.CharField("Название урока RU", max_length=255)
    title_ua = models.CharField("Название урока UA", max_length=255, blank=True)

    description = models.TextField('Описание курса RUS', blank=True, null=True,)
    description_ua = models.TextField('Описание курса UA', blank=True, null=True,)

    class Meta:
        ordering = ['pk']
        verbose_name = "Блок урока"
        verbose_name_plural = "Блоки уроков"

    def __str__(self):
        return self.title
