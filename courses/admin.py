from django.contrib import admin
from .models import CourseItem, CourseProgramTitle


@admin.register(CourseItem)
class AdminCategory(admin.ModelAdmin):
    list_display = ['title', 'hours', 'price_person', 'price_group', 'created', 'visible', ]
    list_filter = ['created', 'title', ]


@admin.register(CourseProgramTitle)
class CourseProgramTitleAdmin(admin.ModelAdmin):
    list_display = ('title', 'course')
    search_fields = ['title']
    list_filter = ['course', ]
