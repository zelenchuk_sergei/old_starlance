from django.contrib import admin
from .models import Callback, EmailLists, CourseOrder


@admin.register(Callback)
class AdminCategory(admin.ModelAdmin):
    list_display = ['name', 'mobile', 'created', ]
    list_filter = ['created', ]


@admin.register(EmailLists)
class AdminCategory(admin.ModelAdmin):
    list_display = ['email', 'created', ]
    list_filter = ['created', ]


@admin.register(CourseOrder)
class AdminCategory(admin.ModelAdmin):
    list_display = ['course', 'name', 'mobile', 'created', 'status' ]
    list_filter = ['created', 'status' ]
