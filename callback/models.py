from django.db import models
from courses.models import CourseItem


class Callback(models.Model):
    name = models.CharField('Имя', max_length=255)
    mobile = models.CharField('Мобильный номер', max_length=255)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name']
        verbose_name = "ЛИД"
        verbose_name_plural = 'ЛИДы'

    def __str__(self):
        return self.name


class EmailLists(models.Model):
    email = models.CharField('Email', max_length=255)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['created']
        verbose_name = "Email"
        verbose_name_plural = 'Emails'

    def __str__(self):
        return self.email


class CourseOrder(models.Model):
    name = models.CharField('Имя', max_length=255)
    mobile = models.CharField("Мобильный", max_length=255)
    email = models.CharField('Email', max_length=255, blank=True)
    course = models.ForeignKey(CourseItem, on_delete=models.CASCADE)
    status = models.BooleanField("Статус заявки", blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['created', ]
        verbose_name = "Заявка на курс"
        verbose_name_plural = "Заявки на курсы"

    def __str__(self):
        return self.name
