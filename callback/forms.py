from django import forms
from .models import Callback, EmailLists, CourseOrder


class CallbackForm(forms.ModelForm):
    class Meta:
        model = Callback
        fields = ('name', 'mobile', )


class EmailLists(forms.ModelForm):
    class Meta:
        model = EmailLists
        fields = ('email', )

class CourseOrder(forms.ModelForm):
	class Meta:
		model = CourseOrder
		fields = ('name', 'mobile', 'email', 'course', )
